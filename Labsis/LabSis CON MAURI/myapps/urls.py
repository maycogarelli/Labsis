
from django.conf.urls import include, url
from django.contrib import admin

import album.views
import album.urls

urlpatterns = [
    url(r'^album/', include('album.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', album.views.home),
]


