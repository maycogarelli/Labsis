# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.contrib import admin
from album.models import Category, Photo
from django.db.models.signals import post_delete
from django.dispatch import receiver

admin.site.register(Category)
admin.site.register(Photo)



