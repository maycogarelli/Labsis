# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from album.models import Category, Photo
from django.views.generic import ListView, DetailView
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView, CreateView, DeleteView




class PhotoUpdate(UpdateView):
    model = Photo


class PhotoCreate(CreateView):
    model = Photo


class PhotoDelete(DeleteView):
    model = Photo
    success_url = reverse_lazy('photo-list')

def home(request):
    return HttpResponse('Bienvenidos a nuestra Pagina!!')

def category(request):
    category_list = Category.objects.all()
    context = {'object_list': category_list}
    return render(request, 'album/category.html', context)

def category_detail(request, category_id):
    category = Category.objects.get(id=category_id)
    context = {'object': category}
    return render(request, 'album/category_detail.html', context)


class PhotoListView(ListView):
    model = Photo

class PhotoDetailView(DetailView):
    model = Photo

def base(request):
    return render(request, 'base.html')
